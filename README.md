# Tiny ribosome profiling dataset

This dataset contains synthetic mRNA sequences (synth.fasta) with their coding sequence annotations (synth.tsv) and simulated footprint reads (fastq/*.fastq). Those files where produced by Carine Legrand for her paper on RiboVIEW available [here](https://academic.oup.com/nar/article/48/2/e7/5645003). The code used to generate those synthetic sequences and footprints is available [here](https://github.com/carinelegrand/RiboVIEW).

## Notes

The fastq files where generated using `bedtools` (v2.26.0 - `bam2fastq`) from BAM files produced by Carine Legrand.
The `contaminants.fasta` file only contains a small region of mRNA0 synthetic mRNA.
